package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	board := [3][3]string{
		{"1", "2", "3"},
		{"4", "5", "6"},
		{"7", "8", "9"},
	}

	ok := true
	for ok == true {
		board = drawBoard(board)
		board, ok = promptForPosition(board)
	}
}

func promptForPosition(pos [3][3]string) ([3][3]string, bool) {
	fmt.Printf("Enter the number of the position you wish to occupy or Q to quit: ")
	reader := bufio.NewReader(os.Stdin)
	move, _ := reader.ReadString('\n')
	move = strings.TrimSuffix(move, "\n")
	moveInt, _ := strconv.Atoi(move)
	if strings.ToUpper(move) == "Q" {
		fmt.Println("You have selected to quit the game")
		return pos, false
	} else if moveInt >= 1 && moveInt <= 9 {
		x, y := convertNumbertoXY(moveInt)
		pos[x][y] = "X"
		return pos, true
	} else {
		fmt.Println("Your choice was out of range")
		return pos, true
	}
	return pos, true
}

func convertNumbertoXY(num int) (int, int) {
	x := (num - 1) / 3
	y := (num - 1) % 3
	return x, y
}

func drawBoard(pos [3][3]string) [3][3]string {
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			fmt.Print(pos[x][y])
			if y != 2 {
				fmt.Print("|")
			}
		}
		fmt.Println()
		if x != 2 {
			fmt.Println("------")
		}
	}
	return pos
}
